require 'test_helper'

class MicropostsControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
  	@micropost = microposts(:orange)
  end

  test "deberia redireccionar el create cuando no se este logeado" do
  	assert_no_difference 'Micropost.count'	do
  		post :create, micropost: { content: "bleeeeeeeeee mihhhhh" }
  	end
  	assert_redirected_to login_url

  end

  test "deberia redireccionar el destroy cuando no se este logeado" do
  	assert_no_difference 'Micropost.count'	do
  		post :destroy, id: @micropost
  	end
  	assert_redirected_to login_url

  end

  test "debe redireccionar DESTROY para un micropost incorrecto" do
    log_in_as(users(:michael))    
    micropost = microposts(:ants)
    assert_no_difference 'Micropost.count' do
      delete :destroy, id: micropost
    end
    assert_redirected_to root_url
  end

end
