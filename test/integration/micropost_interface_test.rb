require 'test_helper'

class MicropostInterfaceTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
  	@user = users(:michael)
  end

  test "interfaz micropost" do
  	log_in_as(@user)
  	get root_path
  	assert_select 'div.pagination'

  	# Valida submision invalida
  	assert_no_difference 'Micropost.count' do
		post microposts_path, micropost: { content: "" }
  	end
  	assert_select 'div#error_explanation'

  	# Submit valido
  	content = "bla bla bla ble ble ble"
  	assert_difference 'Micropost.count', 1 do
  		post microposts_path, micropost: { content: content }
  	end
  	assert_redirected_to root_url
  	follow_redirect!
  	assert_match content, response.body

  	# Borrar un MP
  	assert_select 'a', text: 'borrar'
  	first_micropost = @user.microposts.paginate(page: 1).first
  	assert_difference 'Micropost.count', -1 do
  		delete micropost_path(first_micropost)
  	end

  	# Visita un usuario distinto
  	get user_path(users(:archer))
  	assert_select 'a', text: 'delete', count: 0


  end
end 
