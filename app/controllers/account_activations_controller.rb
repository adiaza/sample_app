class AccountActivationsController < ApplicationController
	def edit
		user = User.find_by(email: params[:email])
		# Check if the user is valid, is not activated, and has a valid activation token
		if user && !user.activated? && user.authenticated?(:activation, params[:id])
			user.activate
			log_in user
			flash[:success] = "Gracias, tu cuenta ha sido activada!"
			redirect_to user
		else
			flash[:danger] = "Link de activación invalido"
			redirect_to root_url
		end
	end
end
