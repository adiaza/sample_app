class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

  def hello
  	render text: "Hello, World!"
  	
  end

  private

  # Confirma un usuario logeado
  def logged_in_user
  	unless logged_in?
  		store_location
  		flash[:danger] = "Porfavor ingresar con tus credenciales"
  		redirect_to login_url
  	end
  end
end
