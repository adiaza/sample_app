class PasswordResetsController < ApplicationController
	before_action :get_user,   		 only: [:edit, :update]
	before_action :valid_user, 		 only: [:edit, :update]
	before_action :check_expiration, only: [:edit, :update]


  def new
  end

  def edit
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = "Te hemos enviado un email con las instrucciones para resetear tu contraseña"
      redirect_to root_url
	else
      flash.now[:danger] = "Email no encontrado"
      render 'new'
    end
  end


  def update
    if password_blank?
      flash.now[:danger] = "La contraseña no debe ser vacía"
      render 'edit'
    elsif @user.update_attributes(user_params)
      log_in @user
      flash[:success] = "La contraseña ha sido reestablecida."
      redirect_to @user
	else
      render 'edit'
    end
  end

  private

  def get_user
  	@user = User.find_by(email: params[:email])
  end

  def valid_user
  	unless (@user && user.activated? && @user.authenticated?(:reset, params[:id]))
  		redirect_to root_url
  	end
  end

  def user_params
  	params.require(:user).permit(:password, :password_confirmation)
  end

  def password_blank?
  	params[:user][:password].blank?
  end

  def check_expiration
    if @user.password_reset_expired?
        flash[:danger] = "La sesion de restablecer contraseña ha expirado"
        redirect_to new_password_reset_url 
    end 
  end

end
