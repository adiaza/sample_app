class UsersController < ApplicationController
  before_action :logged_in_user, only: [ :index, :update, :edit, :destroy, 
                                         :following, :followers ]

  before_action :correct_user, only: [ :update, :edit ]
  before_action :admin_user, only: :destroy

  def index
    @users = User.paginate(page: params[:page])
  end

  def new
  	@user = User.new
  end

  def show
  	@user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def create
  	@user = User.new(user_params)
  	if @user.save
  		# handle a succs save
      @user.send_activation_email
      flash[:info] = "Porfavor revisa tu email, " + @user.email + ", para activar tu cuenta."
      redirect_to root_url
  	else
  		render 'new'
  	end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # send success message and go to profile page
      flash[:success] = "Perfil actualizado."
      redirect_to @user
    else
      # go back to edit page
      render  'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Usuario eliminado"
    redirect_to users_url
  end

  def following
    @title = "Siguiendo"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Seguidores"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

	  def user_params
	  	params.require(:user).permit(:name, :email, :password, :password_confirmation)
	  end

    # Este metodo ahora va en el App Controller ya que es invocado
    # desde multiples controles
    # ------------------------
    #
    # def logged_in_user
    #   unless logged_in?
    #     store_location
    #     flash[:danger] = "Por favor ingresa tus credenciales"
    #     redirect_to login_url
    #   end
    # end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)

    end

    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

end
